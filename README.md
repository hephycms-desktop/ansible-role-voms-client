VOMS client
=========

Installs the IGTF trustanchors aand voms clients. Currently only CMS VO is supported.

https://docs.egi.eu/providers/operations-manuals/howto01_using_igtf_ca_distribution/
https://italiangrid.github.io/voms/

Requirements
------------

EPEL repo enabled

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: desktop
      roles:
         - dietrichliko.voms_client

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
